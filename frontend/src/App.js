import React, {useEffect, useState} from 'react';
import './App.css';
import {BrowserRouter, Switch, Route, NavLink, Link, useRouteMatch, useParams, useHistory} from 'react-router-dom';
import Home from './Home.js';
import Login from './Login.js';
import Create from './Create.js';
import Cards from "./Cards";
import Example from "./Example";
import LocalGame from './LocalGame.js';
import OnlineGame from './OnlineGame.js';

function App() {
  return (
      <div className="App">
        <Display/>
      </div>
  );
}

function Display(){
    return(
      <main>
          <Switch>
              <Route exact path="/" component={Home}/>
              <Route path="/login" component={Login}/>
              <Route path="/cards" component={Cards}/>
              <Route path="/exa" component={Example}/>
              <Route path="/create-account" component={Create}/>
              <Route path="/local-game" component={LocalGame}/>
              <Route path="/online-game" component={OnlineGame}/>
          </Switch>
      </main>
    );
}

export default App;
