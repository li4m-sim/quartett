import React, {useEffect, useState} from 'react';
import "./App.css";
import {Alert, Card, Form, ListGroup, Modal} from "react-bootstrap";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {useHistory} from 'react-router-dom';

export default function LocalGame(){
    const history = useHistory();

    const [playerCounter,  setPlayerCounter] = useState([]);
    const [pcCounter, setPcCounter] = useState([]);
    const [playerOneCard, setPlayerOneCard] = useSessionStorage("CardOne", []);

    const [pcCard, setPcCard] = useSessionStorage("CardTwo", []);
    const [textModal, setTextModal] = useState([]);

    const [showAlert, setShowAlert] = useState(false);
    const [oldCardforModal, setoldCardforModal] = useState([])
    const [playerOldCard, setPlayerOlderCard] =useState([])
    const [textError, setTextError] = useState("Fehler");
    const [userActive, setUserActive] = useState("user1");

    const[attribute, setAttribute] = useState([]);
    const[action, setAction] = useState(2);

    const[attActive, setAttActive] = useState([false, false, false, false, false])

    const [showMessage, setShowMessage] = useState(false);
    const handleClose = () => {
        setShowMessage(false);
        setAttActive([false, false, false, false, false]);
        if(userActive === "user2"){
            onSend();
        }
        //window.location.reload();
    }

    const [attNames, setAttNames] = useState([]);

    const [showEndMessage, setShowEndMessage] = useState(false);

    const [urlPlay, setUrlPlay] = useState("http://localhost:8080/local-game")

    var url = "http://localhost:8080/card-deck";

    useEffect(() => {

        if (playerOneCard.length === 1 || pcCard.length === 1) {
            fetchData();
        }else if(window.localStorage.getItem("fetchNeeded") === "true"){
            fetchData();
        }
    }, [])

    function fetchData() {
        console.log("session " + sessionStorage.getItem("type"))
        fetch(url, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(
                {type: sessionStorage.getItem("type")})
        })
            .then(response => response.json())
            .then(data => {
                console.log(data[0])
                setPlayerOneCard(data[0]);
                setPlayerCounter(playerOneCard.length)
                setPcCard(data[1])
                setPcCounter(pcCard.length)
                setAttNames(data[0].names)
                window.localStorage.setItem("fetchNeeded", "false")
            })
    }

    function onSend(){
        console.log("action " + action)
        console.log("attribute " + attribute)

        var value = {title: "" , actionBtn: "", userActive: ""}

        fetch(urlPlay, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({action, attribute})
        })
            .then(data => data.json())
            .then(data => {
                console.log("url" + urlPlay)
                console.log(data)
                let zw_array = [false, false, false, false, false];
                zw_array[data[0][0].attribute] = true;
                setAttActive(zw_array);
                console.log(data[0][0].cardOld.names)
                setAttNames(data[0][0].cardOld.names)
                console.log(attActive)
                setoldCardforModal(data[0][0].cardOld)
                setPlayerOlderCard(data[0][0].cardOldUser)
                switch(data[0][0].gameStatus){
                    case "0":
                        if(data[0][0].userActive == "user1"){
                           value = {title: "Du hast gewonnen", actionBtn: "Karte einsammeln" ,userActive: "Du bist dran!"}
                           setUrlPlay("http://localhost:8080/local-game");
                           setUserActive("user1");
                        }
                        else if(data[0][0].userActive == "user2"){
                            value = {title: "Du hast gegen den PC verloren", actionBtn: "Karte hergeben", userActive: "Der PC ist jetzt dran"}
                            setUrlPlay("http://localhost:8080/pcPlays");
                            setUserActive("user2");
                        }
                        setPlayerOneCard(data[1][0])
                        setPcCard(data[1][1])
                        setShowMessage(true);
                        break;
                    case "1":
                        if(data[0][0].userActive == "user1"){
                            value = {title: "Du hast gewonnen", actionBtn: "Karte einsammeln" ,userActive: "Du bist dran!"}
                            setUrlPlay("http://localhost:8080/local-game");
                            setUserActive("user1");
                        }
                        else if(data[0][0].userActive == "user2"){
                            value = {title: "Der PC hat gegen dich gewonnen", actionBtn: "Karte hergeben" , userActive: "Der PC ist jetzt dran"}
                            setUrlPlay("http://localhost:8080/pcPlays");
                            setUserActive("user2");
                        }
                        setPlayerOneCard(data[1][0])
                        setPcCard(data[1][1])
                        setShowMessage(true);
                        break;

                    case "2":
                        if(data[0][0].userActive == "user1"){
                            setUrlPlay("http://localhost:8080/local-game");
                            value = {title: "Unentschieden", actionBtn: "Weiterspielen" ,userActive: "Du bist dran!"}
                            setUserActive("user1");
                        }
                        else if(data[0][0].userActive == "user2"){
                            setUrlPlay("http://localhost:8080/pcPlays");
                            value = {title: "Unentschieden", actionBtn: "Weiterspielen" , userActive: "Der PC ist jetzt dran"}
                            setUserActive("user2");
                        }

                        setPlayerOneCard(data[1][0])
                        setPcCard(data[1][1])
                        setShowMessage(true);
                        break;
                    case "3":
                        value = {title: "Das Spiel ist fertig, du hast verloren", actionBtn: "Zurück zum Homescreen", userActive: "Der PC hat gewonnen"}
                        setShowEndMessage(value)
                        setShowEndMessage(true);
                        break;
                    case "4":
                        value = {title: "Das Spiel ist fertig, du hast gewonnen", actionBtn: "Zurück zum Homescreen" ,userActive: "Du hast gewonnen!"}
                        setShowEndMessage(value)
                        setShowEndMessage(true);
                        break;
                }
                console.log(value)
                setTextModal(value)
            })
    }

    function closeEndMsg(){
        setShowEndMessage(false);
        history.push("/");
    }

    return(
        <div >
            <div >
                <Alert show={showAlert} variant="danger" dismissible={true} onClose={() => setShowAlert(false)}>
                    <Alert.Heading>Oopps.. Fehler</Alert.Heading>
                    <p>
                        {textError}
                    </p>
                </Alert>

                <Modal  show={showMessage} onHide={handleClose} >
                    <Modal.Header closeButton>
                        <Modal.Title>{textModal.title}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body >
                        <p>{"Im nächsten Zug: " + textModal.userActive }</p>


                        <div className="card-deck" >
                            <Card className="cardCar">
                                <Card.Body>
                                    <Card.Title className="titleModalCard">{oldCardforModal.name}</Card.Title>
                                    <Card.Img src={oldCardforModal.imagePath}></Card.Img>
                                    <Card.Img className="img">{oldCardforModal.img}</Card.Img>
                                    <ListGroup>
                                        <ListGroup.Item  active={attActive[0]}>
                                            {attNames[0] + " " + oldCardforModal.attribute1}
                                        </ListGroup.Item>
                                        <ListGroup.Item  active={attActive[1]}>
                                            {attNames[1] + " " + oldCardforModal.attribute2}
                                        </ListGroup.Item>
                                        <ListGroup.Item  active={attActive[2]}>
                                            {attNames[2] + " " + oldCardforModal.attribute3}
                                        </ListGroup.Item>
                                        <ListGroup.Item  active={attActive[3]}>
                                            {attNames[3] + " " + oldCardforModal.attribute4}
                                        </ListGroup.Item>
                                        <ListGroup.Item  active={attActive[4]}>
                                            {attNames[4] + " " + oldCardforModal.attribute5}
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Card.Body>
                            </Card>

                            <Card className="cardCar">
                                <Card.Body>
                                    <Card.Title className="titleModalCard">{playerOldCard.name}</Card.Title>
                                    <Card.Img src={playerOldCard.imagePath}></Card.Img>
                                    <Card.Img className="img">{oldCardforModal.img}</Card.Img>
                                    <ListGroup>
                                        <ListGroup.Item active={attActive[0]}>
                                            {attNames[0] + " " + playerOldCard.attribute1}
                                        </ListGroup.Item>
                                        <ListGroup.Item active={attActive[1]}>
                                            {attNames[1] + " " + playerOldCard.attribute2}
                                        </ListGroup.Item>
                                        <ListGroup.Item active={attActive[2]}>
                                            {attNames[2] + " " + playerOldCard.attribute3}
                                        </ListGroup.Item>
                                        <ListGroup.Item active={attActive[3]}>
                                            {attNames[3] + " " + playerOldCard.attribute4}
                                        </ListGroup.Item>
                                        <ListGroup.Item active={attActive[4]}>
                                            {attNames[4] + " " + playerOldCard.attribute5}
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </div>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button  onClick={handleClose}>{textModal.actionBtn}</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={showEndMessage} onHide={closeEndMsg} >
                    <Modal.Header closeButton>
                        <Modal.Title>{textModal.title}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <p>{textModal.userActive }</p>
                        <Card className="cardCar">
                            <Card.Body>
                                <Card.Title className="title">{oldCardforModal.name}</Card.Title>
                                <Card.Img src={oldCardforModal.imagePath}></Card.Img>
                                <Card.Img className="img">{oldCardforModal.img}</Card.Img>
                                <ListGroup>
                                    <ListGroup.Item active={attActive[0]}>
                                        {attNames[0] + " " + oldCardforModal.attribute1}
                                    </ListGroup.Item>
                                    <ListGroup.Item active={attActive[1]}>
                                        {attNames[1] + " " + oldCardforModal.attribute2}
                                    </ListGroup.Item>
                                    <ListGroup.Item active={attActive[2]}>
                                        {attNames[2] + " " + oldCardforModal.attribute3}
                                    </ListGroup.Item>
                                    <ListGroup.Item active={attActive[3]}>
                                        {attNames[3] + " " + oldCardforModal.attribute4}
                                    </ListGroup.Item>
                                    <ListGroup.Item active={attActive[4]}>
                                        {attNames[4] + " " + oldCardforModal.attribute5}
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button  onClick={closeEndMsg}>{textModal.actionBtn}</Button>
                    </Modal.Footer>
                </Modal>

                <Card className="cardCar">
                    <Card.Body>
                        <Card.Title className="title">{playerOneCard.name}</Card.Title>
                        <Card.Img src={playerOneCard.imagePath}></Card.Img>
                        <Card.Img className="img">{playerOneCard.img}</Card.Img>
                        <Form>
                            <ListGroup>
                                <ListGroup.Item eventKey={1} onClick={(e) => setAttribute(0)}>
                                    {attNames[0] + " " + playerOneCard.attribute1}
                                </ListGroup.Item>
                                <ListGroup.Item eventKey={2} onClick={(e) => setAttribute(1)}>
                                    {attNames[1] + " " + playerOneCard.attribute2}
                                </ListGroup.Item>
                                <ListGroup.Item eventKey={3} onClick={(e) => setAttribute(2)}>
                                    {attNames[2] + " " + playerOneCard.attribute3}
                                </ListGroup.Item>
                                <ListGroup.Item eventKey={4} onClick={(e) => setAttribute(3)}>
                                    {attNames[3] + " " + playerOneCard.attribute4}
                                </ListGroup.Item>
                                <ListGroup.Item eventKey={5} onClick={(e) => setAttribute(4)}>
                                    {attNames[4] + " " + playerOneCard.attribute5}
                                </ListGroup.Item>
                            </ListGroup>
                            <FormControl className="OptionBar">
                                <InputLabel id="demo-controlled-open-select-label">Option</InputLabel>
                                <Select value={action} onChange={(e) => setAction(e.target.value)}
                                        labelId="demo-controlled-open-select-label"
                                        id="demo-controlled-open-select">
                                    <MenuItem  value={0}>Higher</MenuItem>
                                    <MenuItem  value={1}>Lower</MenuItem>
                                </Select>
                            </FormControl>
                            <br/><br/>
                            <Button onClick={() => onSend()} variant="contained" color="primary">
                                Send
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        </div>
    );
}

function useSessionStorage(key, initialValue) {
    // State to store our value
    // Pass initial state function to useState so logic is only executed once
    const [storedValue, setStoredValue] = useState(() => {
        try {
            // Get from local storage by key
            const item = window.sessionStorage.getItem(key);
            // Parse stored json or if none return initialValue
            return item ? JSON.parse(item) : initialValue;
        } catch (error) {
            // If error also return initialValue
            console.log(error);
            return initialValue;
        }
    });
    // Return a wrapped version of useState's setter function that ...
    // ... persists the new value to localStorage.
    const setValue = value => {
        try {
            // Allow value to be a function so we have same API as useState
            const valueToStore =
                value instanceof Function ? value(storedValue) : value;
            // Save state
            setStoredValue(valueToStore);
            // Save to local storage
            window.sessionStorage.setItem(key, JSON.stringify(valueToStore));
        } catch (error) {
            // A more advanced implementation would handle the error case
            console.log(error);
        }
    };
    return [storedValue, setValue];
}