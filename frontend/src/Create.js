import React, {useState} from 'react';
import {Modal, Alert} from 'react-bootstrap';
import {NavLink, useHistory} from 'react-router-dom';
import Button from '@material-ui/core/Button';

export default function Login(){
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const history = useHistory();

    const [showAlert, setShowAlert] = useState(false);
    const [checkbox, setCheckbox] = useState([]);

    var urlLogin = "http://localhost:8080/login";
    var url = "http://localhost:8080/newUser";

    function postUser(event){
        event.preventDefault();
        fetch(url, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({username, email, password})
        })
            .then(data => data.json())
            .then(data => {
                console.log(data);
                authentification(event);
            })
            .catch(err => setShowAlert(true)
            )
    };

    function authentification(event) {
        event.preventDefault();
        fetch(urlLogin, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({username, password})
        })
            .then(data => data.json())
            .then(data => {
                console.log("authentification OK")
                if(checkbox == true) {
                    sessionStorage.clear()
                    localStorage.setItem('token', data.token)
                    history.push("/")
                }else{
                    localStorage.clear()
                    sessionStorage.setItem('token', data.token)
                    history.push("/")
                }
            })
            .catch(err => {
                console.log("fehler bei login prozess")
                setShowAlert(true)
            })
    };

    return (
        <div>
            <Alert show={showAlert} variant="danger" dismissible={true} onClose={() => setShowAlert(false)}>
                <Alert.Heading>Oops, Fehler beim Erstellen des Accounts</Alert.Heading>
                <p>
                    Username oder Email existieren bereits!
                </p>
            </Alert>

            <form onSubmit={postUser}>
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Username</label>
                    <input type="text" className="form-control" placeholder="Username" value={username}
                           onChange={event => setUsername(event.target.value)}/>
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="text" className="form-control" placeholder="Email" value={email}
                           onChange={event => setEmail(event.target.value)}/>
                </div>

                <div className="form-group">
                    <label>Passwort</label>
                    <input type="password" className="form-control" placeholder="Passwort"
                           onChange={event => setPassword(event.target.value)} value={password}/>
                </div>

                <div className="form-group">
                    <label>Angemeldet bleiben</label>
                    <input type="checkbox" onChange={event => setCheckbox(event.target.checked)}/>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>);

}