import React, {useEffect, useState} from 'react';

import {ListGroup, Card, Alert, Modal, Form} from "react-bootstrap";
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";


const names = {
    attribute1: "attribute1",
    attribute2: "attribute2",
    attribute3: "attribute3",
    attribute4: "attribute4",
    attribute5: "attribute5"
}
export default function Cards() {

    const [url, setUrl] = useState("http://localhost:8080/getPlayer");
    const [cards, setCards] = useSessionStorage(1, [{}])
    const [cards2, setCards2] = useSessionStorage(2, [{}])
    const [counter, setCounter] = useState(0);
    const [action, setAction] = useState(0);
    const [element, setElement] = useState([])
    const [textModal, setTextModal] = useState([]);
    const [showAlert, setShowAlert] = useState(false);
    const [textError, setTextError] = useState("Fehler");


    const [showMessage, setShowMessage] = useState(false);
    const handleClose = () => {
        setShowMessage(false);
        switch (textModal.id){
            case 0:
                cards.push(cards2[0])
                cards.push(cards[0])
                cards.splice(0, 1);
                cards2.splice(0, 1);
            case 1:
                cards2.push(cards2[0])
                cards2.push(cards[0])
                cards2.splice(0, 1);
                cards.splice(0, 1);
            case 2:
                cards.push(cards[0])
                cards2.push(cards2[0])
                cards2.splice(0, 1);
                cards.splice(0, 1);

        }

    }

    const handleShow = () => setShowMessage(true);


    useEffect(() => {
        if (cards.length === 1 || cards2.length === 1) {
            fetchData();
        }
    }, [])

    function fetchData() {
        fetch(url)
            .then(response => response.json())
            .then(data => {
                setCards(data)
            })
    }

    const processAttribute = (attribute) => {


        console.log("Action gewählt " +  element.action)

        //Wenn als Action "Higher" gewählt hat.
        if (element.action === 0) {

            console.log("In folgenden Aktion sind wir drin  HIGHER" )
            console.log("Mein gewähltes Attribut" +attribute)

            if (cards2[0].attributes[attribute] < cards[0].attributes[attribute]) {

                console.log("Karte gegner: " + cards2[0].attributes[attribute])
                console.log("Eigene Karte: " + cards[0].attributes[attribute])

                console.log("wir sind haben gewonnen, wir sind wirklich higher")

                var value = {id: 0,action: "Höher",title: "Du hast gewonnen", actionString: "höher",  yourAttribut: cards[0].attributes[attribute] ,gegnerAttribut: cards2[0].attributes[attribute]}
                setTextModal(value);

                setShowMessage(true);


                console.log("Länge meiner Karten: " + cards.length)
            } else if (cards2[0].attributes[attribute] > cards[0].attributes[attribute]) {

                console.log("Karte gegner: " + cards2[0].attributes[attribute])
                console.log("Eigene Karte: " + cards[0].attributes[attribute])

                console.log("wir  haben verloren, wir sind nicht higher")

                var value = { id: 1,action: "Höher",title: "Du hast verloren", actionString: "tiefer",  yourAttribut: cards[0].attributes[attribute] ,gegnerAttribut: cards2[0].attributes[attribute]}
                setTextModal(value);

                setShowMessage(true);

                console.log("Länge meiner Karten: " + cards.length)
            } else {

                console.log("wir sind nicht higher haben aber auch nicht verloren -> unentschiedene")
                var value = {id: 2,action: "Höher",title: "Ein Untenschieden", actionString: "gleich",  yourAttribut: cards[0].attributes[attribute] ,gegnerAttribut: cards2[0].attributes[attribute]}

            }

            //Wenn als Action "Lower" gewählt hat.
        } else if (element.action == 1) {
            if (cards2[0].attributes[attribute] > cards[0].attributes[attribute]) {

                console.log("wir haben gewonnen, wir sind wirklich lower")

                var value = {id: 0,action: "Tiefer", title: "Du hast gewonnen", actionString: "tiefer",  yourAttribut: cards[0].attributes[attribute] ,gegnerAttribut: cards2[0].attributes[attribute]}
                setTextModal(value);

                setShowMessage(true);


                console.log(cards.length)
            } else if (cards2[0].attributes[attribute] < cards[0].attributes[attribute]) {

                console.log("wir haben gewonnen, wir sind wirklich lower")

                var value = {id: 0,action: "Tiefer",title: "Du hast gewonnen", actionString: "tiefer",  yourAttribut: cards[0].attributes[attribute] ,gegnerAttribut: cards2[0].attributes[attribute]}
                setTextModal(value);

                setShowMessage(true);

                console.log(cards.length)
            } else {

                console.log("wir sind nicht lower haben aber nicht verloren")
                var value = {id: 2,action: "Tiefer",title: "Ein Untenschieden", actionString: "gleich",  yourAttribut: cards[0].attributes[attribute] ,gegnerAttribut: cards2[0].attributes[attribute]}
                setTextModal(value);

                setShowMessage(true);


            }

        }



    }

    function play() {

        switch (element.name){
            case names.attribute1 :
                console.log("Mit diesem Elememt starten  wir ins Game :")
                console.log("Name :" + element.name)
                console.log("Aktion :" + element.action)


                processAttribute(0);
                break;
            case names.attribute2 :
                console.log("Mit diesem Elememt starten  wir ins Game :")
                console.log("Name :" + element.name)
                console.log("Aktion :" + element.action)

                processAttribute(1);
                break;
            case names.attribute3 :
                console.log("Mit diesem Elememt starten  wir ins Game :")
                console.log("Name :" + element.name)
                console.log("Aktion :" + element.action)

                processAttribute(2);
                break;
            case names.attribute4 :
                console.log("Mit diesem Elememt starten  wir ins Game :")
                console.log("Name :" + element.name)
                console.log("Aktion :" + element.action)

                processAttribute(3);
                break;
            case names.attribute5 :
                console.log("Mit diesem Elememt starten  wir ins Game :")
                console.log("Name :" + element.name)
                console.log("Aktion :" + element.action)

                processAttribute(4);
                break;
        }




    }

    function onClickedItem(props) {
        var value = {name: props.name, card: props.card, action: action}
        setElement(value);


    }

    function onSend(props) {

        if (props.element.name == undefined) {

            if (props.action == 2) {
                setTextError("Bitte wählen Sie eine Option aus.")
            } else {
                setTextError("Bitte wählen Sie einen Wert der Karte aus")
            }

            setShowAlert(true);
        } else {
            setShowAlert(false);

            var finalValue = {element: props.element, action: props.action}
            setElement(finalValue)

            play();

        }
    }

    return (
        <div>
            <div>
                <Alert show={showAlert} variant="danger" dismissible={true} onClose={() => setShowAlert(false)}>
                    <Alert.Heading>Oopps.. Fehler</Alert.Heading>
                    <p>
                        {textError}
                    </p>
                </Alert>

                <Modal show={showMessage} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{textModal.title}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <p>{"Dein Aktion " + textModal.action }</p>
                        <h4>{textModal.yourAttribut + " ist " +textModal.actionString + " als " + textModal.gegnerAttribut}</h4>
                        <Card className="cardCar">
                            <Card.Body>
                                <Card.Title className="title">{cards2[counter].name}</Card.Title>
                                <Card.Img src={cards2[counter].imagePath}></Card.Img>
                                <Card.Img className="img">{cards2[counter].img}</Card.Img>
                                <ListGroup>
                                    <ListGroup.Item>
                                        {"Baujahr " + cards2[counter].attribute1}
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        {"PS: " + cards2[counter].attribute2}
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        {"Hubraum " + cards2[counter].attribute3}
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        {"0 auf 100 " + cards2[counter].attribute4}
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        {"Max Speed " + cards2[counter].attribute5}
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card>


                    </Modal.Body>

                    <Modal.Footer>
                        <Button  onClick={handleClose}>Karte einsammeln</Button>
                    </Modal.Footer>
                </Modal>

                <Card className="cardCar">
                    <Card.Body>
                        <Card.Title className="title">{cards[counter].name}</Card.Title>
                        <Card.Img src={cards[counter].imagePath}></Card.Img>
                        <Card.Img className="img">{cards[counter].img}</Card.Img>
                        <Form>
                        <ListGroup>
                            <ListGroup.Item eventKey={1} onClick={(e) => onClickedItem({
                                name: names.attribute1,
                                card: cards[counter].attribute1
                            })}>
                                {"Baujahr " + cards[counter].attribute1}
                            </ListGroup.Item>
                            <ListGroup.Item eventKey={2} onClick={(e) => onClickedItem({
                                name: names.attribute2,
                                card: cards[counter].attribute2
                            })}>
                                {"PS: " + cards[counter].attribute2}
                            </ListGroup.Item>
                            <ListGroup.Item eventKey={3} onClick={(e) => onClickedItem({
                                name: names.attribute3,
                                card: cards[counter].attribute3
                            })}>
                                {"Hubraum " + cards[counter].attribute3}
                            </ListGroup.Item>
                            <ListGroup.Item eventKey={4} onClick={(e) => onClickedItem({
                                name: names.attribute4,
                                card: cards[counter].attribute4
                            })}>
                                {"0 auf 100 " + cards[counter].attribute4}
                            </ListGroup.Item>
                            <ListGroup.Item eventKey={5} onClick={(e) => onClickedItem({
                                name: names.attribute5,
                                card: cards[counter].attribute5
                            })}>
                                {"Max Speed " + cards[counter].attribute5}
                            </ListGroup.Item>
                        </ListGroup>
                        <FormControl className="OptionBar">
                            <InputLabel id="demo-controlled-open-select-label">Option</InputLabel>
                            <Select value={action} onChange={(e) => setAction(e.target.value)}
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select">
                                <MenuItem  value={0}>Higher</MenuItem>
                                <MenuItem  value={1}>Lower</MenuItem>
                            </Select>
                        </FormControl>
                        <br/><br/>
                        <Button onClick={() => onSend({element, action})} variant="contained" color="primary">
                            Send
                        </Button>
                        </Form>
                    </Card.Body>
                </Card>

            </div>

            <div>
                <Card className="cardCar">
                    <Card.Body>
                        <Card.Title className="title">{cards2[counter].name}</Card.Title>
                        <Card.Img src={cards2[counter].imagePath}></Card.Img>
                        <Card.Img className="img">{cards2[counter].img}</Card.Img>
                        <ListGroup>
                            <ListGroup.Item>
                                {"Baujahr " + cards2[counter].attribute1}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                {"PS: " + cards2[counter].attribute2}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                {"Hubraum " + cards2[counter].attribute3}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                {"0 auf 100 " + cards2[counter].attribute4}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                {"Max Speed " + cards2[counter].attribute5}
                            </ListGroup.Item>
                        </ListGroup>
                    </Card.Body>
                </Card>
            </div>

        </div>

    );
}

function useSessionStorage(key, initialValue) {
    // State to store our value
    // Pass initial state function to useState so logic is only executed once
    const [storedValue, setStoredValue] = useState(() => {
        try {
            // Get from local storage by key
            const item = window.sessionStorage.getItem(key);
            // Parse stored json or if none return initialValue
            return item ? JSON.parse(item) : initialValue;
        } catch (error) {
            // If error also return initialValue
            console.log(error);
            return initialValue;
        }
    });
    // Return a wrapped version of useState's setter function that ...
    // ... persists the new value to localStorage.
    const setValue = value => {
        try {
            // Allow value to be a function so we have same API as useState
            const valueToStore =
                value instanceof Function ? value(storedValue) : value;
            // Save state
            setStoredValue(valueToStore);
            // Save to local storage
            window.sessionStorage.setItem(key, JSON.stringify(valueToStore));
        } catch (error) {
            // A more advanced implementation would handle the error case
            console.log(error);
        }
    };
    return [storedValue, setValue];
}