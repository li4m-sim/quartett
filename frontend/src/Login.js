import React, {useState} from 'react';
import './Login.css';
import {Modal, Alert} from 'react-bootstrap';
import {NavLink, useHistory} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import {makeStyles} from "@material-ui/core/styles";
import Input from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from "@material-ui/core/TextField";

export default function Login(){
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const history = useHistory();

    const [showAlert, setShowAlert] = useState(false);
    const [checkbox, setCheckbox] = useState([]);

    var url = "http://localhost:8080/login"

    function authentification(event) {
        event.preventDefault();
        fetch(url, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({username, password})
        })
            .then(data => data.json())
            .then(data => {
                console.log("authentification OK")
                if(checkbox == true) {
                    sessionStorage.clear()
                    localStorage.setItem('token', data.token)
                    history.push("/")
                }else{
                    localStorage.clear()
                    sessionStorage.setItem('token', data.token)
                    history.push("/")
                }
            })
            .catch(err => setShowAlert(true))
    };

    const useStyles = makeStyles({
        root: {
            background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
            border: 0,
            borderRadius: 10,
            boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
            color: 'white',
            height: 48,
            padding: '0 30px',
        },
    });
    const classes = useStyles();

    return (
        <div>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
            <Alert show={showAlert} variant="danger" dismissible={true} onClose={() => setShowAlert(false)}>
                <Alert.Heading>Oops, Fehler beim Login</Alert.Heading>
                <p>
                    Bei der Anmeldung ist ein Fehler aufgetreten.
                    Falsches Passwort oder Username
                </p>
            </Alert>

            <form onSubmit={authentification}>
                <br/>
                <h3>Sign In</h3>

                <div className="form-group">
                    <TextField type="text" className="form-control" placeholder="Username" value={username}
                           onChange={event => setUsername(event.target.value)}/>
                </div>
                <br/>
                <div className="form-group">
                    <TextField type="password" className="form-control" placeholder="Passwort"
                           onChange={event => setPassword(event.target.value)} value={password}/>
                </div>
                <br/>
                <div className="form-group">
                    <label>Angemeldet bleiben</label>
                    <Checkbox type="checkbox" onChange={event => setCheckbox(event.target.checked)} color="primary"/>
                </div>
                <Button type="submit" className={classes.root}>Submit</Button>
            </form>
            <br />
            <Button className={classes.root} onClick={() => useHistory.push("/create-account")}>Create Account</Button>
        </div>);

}