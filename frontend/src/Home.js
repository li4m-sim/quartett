import React, {useEffect, useState} from 'react';
import './Home.css';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import NavLink from "react-bootstrap/NavLink";
import TextField from "@material-ui/core/TextField";
import Modal from "react-bootstrap/Modal";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 10,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
});

function NewGameButton() {
    const classes = useStyles();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const history = useHistory();

    function newGamePcCar() {
        handleClose();
        window.localStorage.setItem("fetchNeeded", "true");
        window.sessionStorage.setItem("type", "car")
        history.push("/local-game")
    }

    function newGamePcBike() {
        handleClose();
        window.localStorage.setItem("fetchNeeded", "true");
        window.sessionStorage.setItem("type", "bike")
        history.push("/local-game")
    }

    function newGamePcSoccer() {
        handleClose();
        window.localStorage.setItem("fetchNeeded", "true");
        window.sessionStorage.setItem("type", "soccer")
        history.push("/local-game")
    }

    function newGameOnline() {
        handleClose();
        history.push("/online-game")
    }

    return (
        <>
            <div>
                <link rel="stylesheet"
                      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"/>
                <Button onClick={handleShow} className={classes.root}>New Game</Button>
                <Modal show={show} onHide={handleClose} backdrop={"static"}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Choose your Game
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Footer class=" modal-lg">
                        <div class="container">
                            <img src="/startpics/bikes.png" alt="bike"/>
                            <Button class="btn" onClick={newGamePcBike}>Bikes</Button>
                        </div>
                        <div class="container">
                            <img src="/startpics/players.png" alt="player"/>
                            <Button class="btn" onClick={newGamePcSoccer}>Players</Button>
                        </div>
                        <div class="container">
                            <img src="/startpics/cars.png" alt="car"/>
                            <Button class="btn" onClick={newGamePcCar}>Cars</Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        </>
    );
}

export default function Home() {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div style={{backgroundColor: "white"}}>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"/>
            <br/><br/>
            <Button variant="contained" color="primary" onClick={() => history.push('/login')}
                    className={classes.root}>Login</Button>
            <br/><br/><br/><br/>
            <NewGameButton/>
            <br/><br/><br/><br/>
            <Button variant="contained" color="secondary" className={classes.root}>
                Join Game
            </Button>
            <br/>
            <TextField/>
        </div>
    );
}
