package ch.axa.rest.model;

public class Stats {

    private int plays;
    private int wins;
    private int loses;


    public Stats() {
        this.plays = 0;
        this.wins = 0;
        this.loses = 0;
    }


    public double getWinRate(){
        return wins/plays * 100;
    }

    public void addWin(){
        this.wins++;
    }

    public void addLose(){
        this.loses++;
    }

    public void addGame(){
        this.plays++;
    }

    @Override
    public String toString(){
        return "Anzahl Spiele: " + plays + " davon hat er " + wins + " gewonnen, er hat also eine Gewinchance von " + getWinRate() + "%";
    }


    public int getPlays() {
        return plays;
    }

    public void setPlays(int plays) {
        this.plays = plays;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLoses() {
        return loses;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

}
