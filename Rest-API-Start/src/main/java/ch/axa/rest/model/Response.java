package ch.axa.rest.model;

import java.util.ArrayList;
import java.util.List;

public class Response {

    private String userActive;
    private String gameStatus;
    private Card cardOld;
    private Card cardOldUser;
    private Integer attribute;
    private List<Integer> sizesLists;


    public Response(String userActive, String gameStatus, Card cardOld, Card cardOldUser) {
        this.userActive = userActive;
        this.gameStatus = gameStatus;
        this.cardOld = cardOld;
        this.cardOldUser = cardOldUser;
        this.attribute = -1;
        this.sizesLists = new ArrayList<>();
    }


    public String getUserActive() {
        return userActive;
    }

    public void setUserActive(String userActive) {
        this.userActive = userActive;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public Card getCardOld() {
        return cardOld;
    }

    public void setCardOld(Card cardOld) {
        this.cardOld = cardOld;
    }

    public Card getCardOldUser() {
        return cardOldUser;
    }

    public void setCardOldUser(Card cardOldUser) {
        this.cardOldUser = cardOldUser;
    }

    public Integer getAttribute() {
        return attribute;
    }

    public void setAttribute(Integer attribute) {
        this.attribute = attribute;
    }

    public List<Integer> getSizesLists() {
        return sizesLists;
    }

    public void setSizesLists(List<Integer> sizesLists) {
        this.sizesLists = sizesLists;
    }
}
