package ch.axa.rest.model;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class



QuartettRepositoryMock {
    private String turn = "user1";

    private List<Card> user1;
    private List<Card> user2;

    private List<Card> cardsCar;
    private List<Card> cardsBike;
    private List<Card> cardsPlayer;

    private Quartett qCars;
    private Quartett qBikes;
    private Quartett qPlayers;

    private List<Integer> sizesList;

    List<Double> averageAttributes;

    public QuartettRepositoryMock() {
        cardsCar = CardData.CARCARDS;
        cardsBike = CardData.BIKECARDS;
        cardsPlayer = CardData.PLAYERCARDS;

        this.qCars = new Quartett("Cars", cardsCar);
        this.qBikes = new Quartett("Bikes", cardsBike);
        this.qPlayers = new Quartett("Players", cardsPlayer);
    }

    public Optional<Card> getCardById(int id) {
        return cardsCar.stream().filter(x -> x.getId() == id).findFirst();
    }

    public List<Card> getCardsforUser(String type) {
        System.out.println("methode ausgeführt");
        List<Card> li = new ArrayList<>();
        List<Integer> numbers = new ArrayList<>();

        user1 = new ArrayList<>();
        user2 = new ArrayList<>();

        for (int i = 0; numbers.size() < 25; i++) {
            int random = new Random().nextInt(50);
            if (!numbers.contains(random)) {
                numbers.add(random);
            }
        }


            switch (type){
                case "car":
                    for(int number: numbers) {
                        user1.add(cardsCar.get(number));
                    }
                    for(int i = 0; i < 50; i++){
                        if(numbers.contains(i)){
                            //pass
                        }else{
                            user2.add(cardsCar.get(i));
                            System.out.println("neuer eintrag car " + user2.size());
                        }

                        averageAttributes = qCars.getAverageAttributes();
                    }
                    break;
                case "bike":
                    //add bike cards
                    for(int number: numbers) {
                        user1.add(cardsBike.get(number));
                    }
                    for(int i = 0; i < 50; i++){
                        if(numbers.contains(i)){
                            //pass
                        }else{
                            user2.add(cardsBike.get(i));
                            System.out.println("neuer eintrag bike " + user2.size());
                        }

                        averageAttributes = qBikes.getAverageAttributes();
                    }
                    break;
                case "soccer":
                    //add soccer cards
                    for(int number: numbers) {
                        user1.add(cardsPlayer.get(number));
                    }
                    for(int i = 0; i < 50; i++){
                        if(numbers.contains(i)){
                            //pass
                        }else{
                            user2.add(cardsPlayer.get(i));
                            System.out.println("neuer eintrag soccer" + user2.size());
                        }
                    }
                    averageAttributes = qPlayers.getAverageAttributes();
                    break;
            }


        li.add(user1.get(0));
        li.add(user2.get(0));

        return li;
    }

    public List<List> play(Integer action, Integer attribute){

        Response re;
        if(turn.equals("user1")){
            if(action == 0){
                if(user1.get(0).getAttributes().get(attribute) > user2.get(0).getAttributes().get(attribute)){
                    //turn stays the same, return info that user1 won and add Cards to user1
                    // 1 = won
                    re = new Response(turn, "1", user2.get(0), user1.get(0));
                    user1Won();
                }else if(user1.get(0).getAttributes().get(attribute) < user2.get(0).getAttributes().get(attribute)){
                    //turn changes, return info that user1 lost and add Cards to user2
                    // 0 = lost
                    turn = "user2";
                    re = new Response(turn, "0", user2.get(0), user1.get(0));
                    user2Won();
                }else{
                    //if no one won return cards to the end, return info and turn changes
                    turn = "user2";
                    re = new Response(turn, "2", user2.get(0), user1.get(0));
                    noOneWon();
                }
            }else{
                if(user1.get(0).getAttributes().get(attribute) > user2.get(0).getAttributes().get(attribute)){
                    //turn stays the same, return info that user1 won and add Cards to user1
                    // 0 = lost
                    turn = "user2";
                    re = new Response(turn, "0", user2.get(0), user1.get(0));
                    user2Won();


                }else if(user1.get(0).getAttributes().get(attribute) < user2.get(0).getAttributes().get(attribute)){
                    //turn changes, return info that user1 lost and add Cards to user2
                    // 1 = won
                    re = new Response(turn, "1", user2.get(0), user1.get(0));
                    user1Won();

                }else{
                    //if no one won return cards to the end, return info and turn changes
                    turn = "user2";
                    re = new Response(turn, "2", user2.get(0), user1.get(0));
                    noOneWon();
                }
            }
        }else{
            if(action == 0){
                if(user2.get(0).getAttributes().get(attribute) > user1.get(0).getAttributes().get(attribute)){
                    //turn stays the same, return info that user1 won and add Cards to user1
                    // 1 = won
                    re = new Response(turn, "1", user2.get(0), user1.get(0));
                    user2Won();
                }else if(user2.get(0).getAttributes().get(attribute) < user1.get(0).getAttributes().get(attribute)){
                    //turn changes, return info that user1 lost and add Cards to user2
                    // 0 = lost
                    turn = "user1";
                    re = new Response(turn, "0", user2.get(0), user1.get(0));
                    user1Won();

                }else{
                    //if no one won return cards to the end, return info and turn changes
                    turn = "user1";
                    re = new Response(turn, "2", user2.get(0), user1.get(0));
                    noOneWon();
                }
            }else{
                if(user1.get(0).getAttributes().get(attribute) > user2.get(0).getAttributes().get(attribute)){
                    //turn stays the same, return info that user1 won and add Cards to user1
                    // 0 = lost
                    re = new Response(turn, "1", user2.get(0), user1.get(0));
                    user2Won();

                }else if(user1.get(0).getAttributes().get(attribute) < user2.get(0).getAttributes().get(attribute)){
                    //turn changes, return info that user1 lost and add Cards to user2
                    // 1 = won
                    turn = "user1";
                    re = new Response(turn, "0", user2.get(0), user1.get(0));
                    user1Won();
                }else{
                    //if no one won return cards to the end, return info and turn changes
                    turn = "user1";
                    re = new Response(turn, "2", user2.get(0), user1.get(0));
                    noOneWon();
                }
            }
        }
        List<Card> cards = new ArrayList<>();
        if (user1.size() == 0) {
            re.setGameStatus("3");
        } else {
            cards.add(user1.get(0));
        }
        if(user2.size() == 0){
            re.setGameStatus("4");
        }else{
            cards.add(user2.get(0));
        }

        sizesList = new ArrayList<>();

        sizesList.add(0, user1.size());
        sizesList.add(1, user2.size());

        System.out.println("use1 " + user1.size());
        System.out.println("user2 " + user2.size());
        re.setSizesLists(sizesList);

        List<Response> responsesList = new ArrayList<>();
        re.setAttribute(attribute);
        responsesList.add(re);
        List<List> relist = new ArrayList<>();
        relist.add(0, responsesList);
        relist.add(1, cards);
        return relist;
    }

    public void user1Won(){
        user1.add(user1.get(0));
        user1.add(user2.get(0));
        user1.remove(0);
        user2.remove(0);
    }

    public void user2Won(){
        user2.add(user2.get(0));
        user2.add(user1.get(0));
        user2.remove(0);
        user1.remove(0);
    }

    public void noOneWon(){
        user1.add(user1.get(0));
        user1.remove(0);
        user2.add(user2.get(0));
        user2.remove(0);
    }

    public List<List> playPc() {
        Random random = new Random();
        Integer action = random.nextInt(1);
        Integer attribute = random.nextInt(4);
        return play(action, attribute);
    }

    public List<List> playPcClever(){
        System.out.println("Pc Clever");

        Random random = new Random();
        Integer attribute = random.nextInt(4);
        Integer action;

        if(averageAttributes.get(attribute) > user2.get(0).getAttributes().get(attribute)){
            action = 1;
        }
        else if(averageAttributes.get(attribute) < user2.get(0).getAttributes().get(attribute)){
            action = 0;
        }else{
            action = random.nextInt(1);
        }

        return play(action, attribute);
    }
}




