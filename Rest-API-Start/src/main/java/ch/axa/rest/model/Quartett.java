package ch.axa.rest.model;

import java.util.ArrayList;
import java.util.List;

public class Quartett {

    private static int lastid;
    private int id;
    private String name;
    private List<Card> cards;

    public Quartett(String name, List<Card> cards){
        this.id = generatedLastId();
        this.name = name;
        this.cards = cards;
    }

    public List<Double> getAverageAttributes(){
        List<Double> li = new ArrayList<>();
        double attribute1 = 0;
        double attribute2 = 0;
        double attribute4 = 0;
        double attribute3 = 0;
        double attribute5 = 0;

        for (Card c : cards) {
            attribute1 = attribute1 + c.getAttribute1();
            attribute2 = attribute2 + c.getAttribute2();
            attribute3 = attribute3 + c.getAttribute3();
            attribute4 = attribute4 + c.getAttribute4();
            attribute5 = attribute5 + c.getAttribute5();
        }

        li.add(attribute1 / cards.size());
        li.add(attribute2 / cards.size());
        li.add(attribute3 / cards.size());
        li.add(attribute4 / cards.size());
        li.add(attribute5 / cards.size());

        return li;
    }

    private int generatedLastId() {
        return lastid++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}
