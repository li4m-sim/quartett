package ch.axa.rest.model;

public class Info {

    private Integer action;
    private Integer attribute;

    public Info(Integer action, Integer attribute){
        this.action = action;
        this.attribute = attribute;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Integer getAttribute() {
        return attribute;
    }

    public void setAttribute(Integer attribute) {
        this.attribute = attribute;
    }
}
