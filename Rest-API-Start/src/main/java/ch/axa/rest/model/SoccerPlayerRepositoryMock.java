package ch.axa.rest.model;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class SoccerPlayerRepositoryMock {

    private List<Card> soccerPlayers;

    private List<Card> user1;
    private List<Card> user2;

    public SoccerPlayerRepositoryMock() {
        List<String> attNamesSoccer = Arrays.asList("Groesse:", "Schnelligkeit:", "Marktwert (Mio.):", "Alter:", "Bewertung:");
        this.soccerPlayers = new ArrayList<>();
        soccerPlayers.addAll(Arrays.asList(
                //Attribute 1: Grösse
                //Attribute 2: Pace
                //Attribute 3: Marktwerkt in Mio
                //Attribute 4: Alter
                //Attribute 5: Bewertung

                new Card("Lionel Messi", 170, 85, 103, 33, 93, "/players/lionelmessi.jpg", attNamesSoccer),
                new Card("Cristiano Ronaldo", 187, 89, 63, 35, 92, "/players/cr7.jpg", attNamesSoccer),
                new Card("Jan Oblak", 188, 10, 100, 27, 90, "/players/janoblak.jpg", attNamesSoccer),
                new Card("Neymar Jr", 175, 91, 132, 28, 91, "/players/neymarjr.jpg", attNamesSoccer),
                new Card("Kevin de Bruyne", 181, 76, 129, 29, 91, "/players/kevindebruyne.jpg", attNamesSoccer),

                new Card("Robert Lewandowski", 184, 78, 111, 31, 91, "/players/robertlewa.jpg", attNamesSoccer),
                new Card("Kylian Mbappé", 178, 96, 185, 21, 90, "/players/kilianmbappe.jpg", attNamesSoccer),
                new Card("Mohamed Salah", 175, 93, 120, 28, 90, "/players/mosalah.jpg", attNamesSoccer),
                new Card("Harry Kane", 188, 68, 123, 26, 89, "/players/harrykane.jpg", attNamesSoccer),
                new Card("Casemiro", 185, 65, 90, 28, 89, "/players/casemiro.jpg", attNamesSoccer),


                new Card("Karim Benzema", 185, 74, 83, 32, 88, "/players/karimbenzema.jpg", attNamesSoccer),
                new Card("Paulo Dybala", 177, 85, 109, 26, 88, "/players/pauladybala.jpg", attNamesSoccer),
                new Card("Eden Hazard", 175, 88, 88, 29, 87, "/players/edenhazard.jpg", attNamesSoccer),
                new Card("Jadon Sancho", 180, 83, 124, 20, 87, "/players/jadonsancho.jpg", attNamesSoccer),
                new Card("Fabinho", 188, 67, 88, 26, 86, "/players/fabinho.jpg", attNamesSoccer),

                new Card("Erling Haaland", 194, 85, 92, 19, 84, "/players/erlinghaland.jpg", attNamesSoccer),
                new Card("Leroy Sane", 184, 93, 81, 24, 85, "/players/leroysane.jpg", attNamesSoccer),
                new Card("Timo Werner", 180, 91, 74, 24, 85, "/players/timowerner.jpg", attNamesSoccer),
                new Card("Mauro Icardi", 181, 73, 63, 27, 84, "/players/mauraicardi.jpg", attNamesSoccer),
                new Card("Lorenzo Insigne", 163, 88, 53, 29, 84, "/players/lorenzoinsigne.jpg", attNamesSoccer),

                new Card("Ousmane Dembélé", 178, 92, 57, 23, 83, "/players/ousmanedebele.jpg", attNamesSoccer),
                new Card("Joe Gomez", 188, 82, 49, 23, 83, "/players/joegomez.jpg", attNamesSoccer),
                new Card("Nelson Semedo", 177, 91, 39, 26, 83, "/players/nelsonsemedo.jpg", attNamesSoccer),
                new Card("Denis Zakaria ", 189, 83, 47, 23, 83, "/players/deniszakaria.jpg", attNamesSoccer),
                new Card("Gabriel Jesus", 175, 85, 52, 23, 83, "/players/gabireljesus.jpg", attNamesSoccer),

                new Card("João Félix", 181, 81, 67, 20, 82, "/players/joaofelix.jpg", attNamesSoccer),
                new Card("Florian Thauvin", 179, 80, 32, 27, 82, "/players/thauvin.jpg", attNamesSoccer),
                new Card("Richarlison", 184, 83, 52, 23, 81, "/players/richarlison.jpg", attNamesSoccer),
                new Card("Aleksandar Kolarov", 187, 68, 9, 34, 81, "/players/alexandrkolarov.jpg", attNamesSoccer),
                new Card("Max Kruse", 180, 65, 20, 32, 81, "/players/maxkruse.jpg", attNamesSoccer),


                new Card("Leon Bailey", 178, 94, 32, 22, 80, "/players/leonbailey.jpg", attNamesSoccer),
                new Card("Arjen Robben", 180, 74, 7, 36, 80, "/players/arjenrob.jpg", attNamesSoccer),
                new Card("Vitolo", 185, 77, 17, 30, 79, "/players/vitolo.jpg", attNamesSoccer),
                new Card("Julian Weigl", 185, 59, 24, 24, 79, "/players/julianweigl.jpg", attNamesSoccer),
                new Card("Olivier Giroud", 193, 39, 11, 33, 79, "/players/giroud.jpg", attNamesSoccer),

                new Card("Marvin Hitz", 192, 10, 7, 32, 78, "/players/marvinhitz.jpg", attNamesSoccer),
                new Card("Sokratis ", 186, 74, 7, 36, 78, "/players/sokratis.jpg", attNamesSoccer),
                new Card("Jose Fonte", 191, 32, 2, 36, 78, "/players/josefonte.jpg", attNamesSoccer),
                new Card("Gervinho", 179, 90, 9, 33, 78, "/players/gervinho.jpg", attNamesSoccer),
                new Card("Manuel Akanji", 187, 77, 20, 32, 78, "/players/manuelakanji.jpg", attNamesSoccer),


                new Card("Sandro Tonali", 182, 80, 24, 20, 77, "/players/sandortonali.jpg", attNamesSoccer),
                new Card("Dante", 189, 33, 1, 36, 76, "/players/dante.jpg", attNamesSoccer),
                new Card("Rodrigo Palacio", 176, 72, 2, 38, 75, "/players/palacio.jpg", attNamesSoccer),
                new Card("Valon Behrami", 184, 56, 1, 35, 74, "/players/behrami.jpg", attNamesSoccer),
                new Card("Daniel Welbeck", 185, 74, 3, 29, 73, "/players/welbeck.jpg", attNamesSoccer),

                new Card("Angel Di Maria", 180, 83, 63, 32, 87, "/players/angeldimaria.jpg", attNamesSoccer),
                new Card("Paul Pogba", 191, 73, 77, 27, 86, "/players/pogba.jpg", attNamesSoccer),
                new Card("Marco Reus", 180, 80, 44, 31, 85, "/players/reus.jpg", attNamesSoccer),
                new Card("Marcus Rashford", 180, 91, 86, 22, 85, "/players/rashford.jpg", attNamesSoccer),
                new Card("Kai Havertz", 189, 84, 120, 21, 84, "/players/maxkruse.jpg", attNamesSoccer)

                ));
        new Quartett("Soccers", soccerPlayers);
    }

    public Optional<Card> getCardById(int id) {
        return soccerPlayers.stream().filter(x -> x.getId() == id).findFirst();

    }

    public List<Card> getCardsCar() {
        return soccerPlayers;
    }



    public List<List> getCardsforUser() {
        System.out.println("methode ausgeführt");
        List<List> li = new ArrayList<>();
        List<Integer> numbers = new ArrayList<>();

        user1 = new ArrayList<>();
        user2 = new ArrayList<>();

        for (int i = 0; numbers.size() < 25; i++) {
            int random = new Random().nextInt(50);
            if (!numbers.contains(random)) {
                numbers.add(random);
            }
        }

        for(int number : numbers){
            user1.add(soccerPlayers.get(number));
        }

        for(int i = 0; i < 50; i++){
            if(numbers.contains(i)){

            }else{
                user2.add(soccerPlayers.get(i));
            }
        }

        li.add(user1);
        li.add(user2);

        return li;
    }


    public List<Card> getUser1() {
        return user1;
    }

    public List<Card> getUser2() {
        return user2;
    }
}




