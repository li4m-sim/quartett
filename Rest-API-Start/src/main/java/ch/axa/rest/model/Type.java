package ch.axa.rest.model;

public class Type {

    private String type;

    public String getType(){
        return type;
    }

    @Override
    public String toString() {
        return "type='" + type;
    }
}
