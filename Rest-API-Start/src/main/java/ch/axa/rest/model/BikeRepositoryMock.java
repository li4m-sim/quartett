package ch.axa.rest.model;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class BikeRepositoryMock {

    private List<Card> bikes;

    private List<Card> user1;
    private List<Card> user2;

    public BikeRepositoryMock() {
        List<String> attNamesBike = Arrays.asList("Preis:", "Federweg:", "Radgroesse:", "Jahrgang:", "Sterne Bewertung:");
        this.bikes = new ArrayList<>();
        bikes.addAll(Arrays.asList(
                //Attribute 1: preis
                //Attribute 2: federweg
                //Attribute 3: radgrösse
                //Attribute 4: Jahrgang
                //Attribute 5: Star rating out of 5

                new Card("Yeti SB150", 9100, 150, 29, 2021,  4.9, "/bike-pictures/Yeti-SB150.jpg", attNamesBike),
                new Card("Yeti SB115", 8100, 115, 29, 2021, 4.9,"/bike-pictures/Yeti-SB115.jpg", attNamesBike),
                new Card("Yeti SB165", 9300, 165, 27.5, 2021, 4.9,"/bike-pictures/Yeti-SB165.jpg", attNamesBike),
                new Card("Yeti-SB140", 8700, 140, 27.5, 2021, 5.0,"/bike-pictures/Yeti-SB140.jpg", attNamesBike),
                new Card("Trek Slash", 10599, 160, 29, 2021, 4.9,"/bike-pictures/Trek-Slash.jpg", attNamesBike),
                new Card("Trek Fuel", 9599, 130, 29, 2021, 4.8,"/bike-pictures/Trek-Fuel.jpg", attNamesBike),
                new Card("Trek Remedy", 5899, 160, 29, 2021 ,4.1,"/bike-pictures/Trek-Remedy.jpg", attNamesBike),
                new Card("Trek Session", 12199, 210, 27.5, 2021 ,4.9,"/bike-pictures/Trek-Session.jpg", attNamesBike),
                new Card("Specialized Turbo Levo SL", 14999, 150, 29, 2021 ,4.8,"/bike-pictures/Specialized-Turbo-Levo-SL.jpg", attNamesBike),
                new Card("Specialized Enduro S-Works", 12999, 170, 29, 2021, 5.0,"/bike-pictures/Specialized-Enduro-S-Works.jpg", attNamesBike),
                new Card("Specialized Demo", 8299, 200, 29, 2021, 4.6,"/bike-pictures/Specialized-Demo.jpg", attNamesBike),
                new Card("Santa Cruz Bullit", 12290, 170, 29, 2021, 4.9,"/bike-pictures/Santa-Cruz-Bullit.jpg", attNamesBike),
                new Card("Santa Cruz Bronson", 12190, 150, 27.5, 2021,  3.0,"/bike-pictures/Santa-Cruz-Bronson.jpg", attNamesBike),
                new Card("Santa Cruz Heckler", 11490, 150, 27.5, 2021, 3.7,"/bike-pictures/Santa-Cruz-Heckler.jpg", attNamesBike),
                new Card("Santa Cruz Tallboy", 7890, 120, 29, 2021, 4.0,"/bike-pictures/Santa-Cuz-Tallboy.jpg", attNamesBike),
                new Card("Santa Cruz Hightower", 11790, 140, 29, 2021, 4.5,"/bike-pictures/Santa-Cruz-Hightower.jpg", attNamesBike),
                new Card("Santa Cruz Megatower", 11890, 160, 29, 2021, 4.5,"/bike-pictures/Santa-Cruz-Megatower.jpg", attNamesBike),
                new Card("Santa Cruz V10", 9290, 215, 29, 2021, 4.4,"/bike-pictures/Santa-Cruz-V10.jpg", attNamesBike),
                new Card("Rocky Mountain Instinct", 10000, 140, 29, 2021, 4.3,"/bike-pictures/Rocky-Mountain-Instinct.jpg", attNamesBike),
                new Card("Rocky Mountain Instinct Carbon", 8000, 155, 29, 2021, 4.9,"/bike-pictures/Rocky-Mountain-Instinct-Carbon.jpg", attNamesBike),
                new Card("Rocky Mountain Altitude", 11500, 160, 29, 2021, 4.9,"/bike-pictures/Rocky-Mountain-Altitude.jpg", attNamesBike),
                new Card("Rocky Mountain Slayer", 9200, 170, 29, 2021, 4.7,"/bike-pictures/Rocky-Mountain-Slayer.jpg", attNamesBike),
                new Card("Pivot Switchblade", 6799, 142, 29, 2021, 4.9,"/bike-pictures/Pivot-Switchblade.jpg", attNamesBike),
                new Card("Pivot Firebird", 6899, 162, 29, 2021, 4.9,"/bike-pictures/Pivot-Firebird.jpg", attNamesBike),
                new Card("Pivot Mach 5.5", 6699, 140, 27.5, 2021, 4.9,"/bike-pictures/Pivot-Mach-5.5.jpg", attNamesBike),
                new Card("Pivot Phoenix", 7599, 190, 29, 2021, 4.8,"/bike-pictures/Pivot-Phoenix.jpg", attNamesBike),
                new Card("YT Industries Jeffsy", 5299, 150, 29, 2021, 4.2,"/bike-pictures/YT-Jeffsy.jpg", attNamesBike),
                new Card("YT Industries Decoy", 6999, 150, 29, 2021, 4.5,"/bike-pictures/YT-Decoy.jpg", attNamesBike),
                new Card("YT Industries Capra", 5999, 170, 29, 2021, 4.5,"/bike-pictures/YT-Capra.jpg", attNamesBike),
                new Card("YT Industries Tues", 5499, 200, 29, 2021, 4.5,"/bike-pictures/YT-Tues.jpg", attNamesBike),
                new Card("Commencal Meta TR 29", 4299, 140, 29, 2021, 4.6,"/bike-pictures/Commencal-Meta-TR29.jpg", attNamesBike),
                new Card("Commencal Meta AM 29", 5299, 160, 29, 2021, 4.6,"/bike-pictures/Commencal-Meta-AM29.jpg", attNamesBike),
                new Card("Commencal Clash", 4999, 170, 27.5, 2021, 3.2,"/bike-pictures/Commencal-Clash.jpg", attNamesBike),
                new Card("Commencal Furious", 4799, 200, 27.5, 2021, 4.8,"/bike-pictures/Commencal-Furious.jpg", attNamesBike),
                new Card("Commencal Supreme DH 29", 5799, 200, 29, 2021, 3.9,"/bike-pictures/Commencal-Supreme-DH29.jpg", attNamesBike),
                new Card("Canyon Spectral", 4399, 150, 27.5, 2021, 4.5,"/bike-pictures/Canyon-Spectral.jpg", attNamesBike),
                new Card("Canyon Torque", 5099, 170, 27.5, 2021, 4.3,"/bike-pictures/Canyon-Torque.jpg", attNamesBike),
                new Card("Canyon Strive", 6049, 160, 29, 2021, 4.1,"/bike-pictures/Canyon-Strive.jpg", attNamesBike),
                new Card("Canyon Sender", 5899, 200, 29, 2021, 4.2,"/bike-pictures/Canyon-Sender.jpg", attNamesBike),
                new Card("Antidote Carbonjack 29", 20000, 150, 29, 2021, 5.0,"/bike-pictures/Antidote-Carbonjack-29.jpg", attNamesBike),
                new Card("Orbea Rallon", 8999, 160, 29, 2021, 4.8,"/bike-pictures/Orbea-Rallon.jpg", attNamesBike),
                new Card("Orbea Occam", 7999, 140, 29, 2021, 4.4,"/bike-pictures/Orbea-Occam.jpg", attNamesBike),
                new Card("Ibis Ripmo", 6590, 142, 29, 2021, 3.6,"/bike-pictures/Ibis-Ripmo.jpg", attNamesBike),
                new Card("Ibis Ripley", 5699, 150, 29, 2021, 3.8,"/bike-pictures/Ibis-Ripley.jpg", attNamesBike),
                new Card("Specialized Kenevo Expert", 8499, 180, 27.5, 2021, 4.4,"/bike-pictures/Specialized-Kenevo-Expert.jpg", attNamesBike),
                new Card("Stevens Tanowha", 800, 90, 27.5, 2020, 2.5,"/bike-pictures/Stevens-Tanowha.jpg", attNamesBike),
                new Card("Stevens Colorado", 2500, 100, 29, 2021, 3.1,"/bike-pictures/Stevens-Colorado.jpg", attNamesBike),
                new Card("Juliana Maverick", 8590, 140, 29, 2021, 4.4,"/bike-pictures/Juliana-Maverick.jpg", attNamesBike),
                new Card("Juliana Furtado", 8290, 130, 29, 2021, 4.5,"/bike-pictures/Juliana-Furtado.jpg", attNamesBike),
                new Card("Juliana Joplin", 8390, 120, 29, 2021, 4.6,"/bike-pictures/Juliana-Joplin.jpg", attNamesBike)

        ));
        new Quartett("Soccers", bikes);
    }

    public Optional<Card> getCardById(int id) {
        return bikes.stream().filter(x -> x.getId() == id).findFirst();

    }

    public List<Card> getCardsCar() {
        return bikes;
    }



    public List<List> getCardsforUser() {
        System.out.println("methode ausgeführt");
        List<List> li = new ArrayList<>();
        List<Integer> numbers = new ArrayList<>();

        user1 = new ArrayList<>();
        user2 = new ArrayList<>();

        for (int i = 0; numbers.size() < 25; i++) {
            int random = new Random().nextInt(50);
            if (!numbers.contains(random)) {
                numbers.add(random);
            }
        }

        for(int number : numbers){
            user1.add(bikes.get(number));
        }

        for(int i = 0; i < 50; i++){
            if(numbers.contains(i)){

            }else{
                user2.add(bikes.get(i));
            }
        }

        li.add(user1);
        li.add(user2);

        return li;
    }


    public List<Card> getUser1() {
        return user1;
    }

    public List<Card> getUser2() {
        return user2;
    }
}




