package ch.axa.rest.model;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private static int lastId = 0;
    private int id;
    private String username;
    private String email;
    private String password_hash;
    private String authentication_token;
    private Stats stats;
    private List<Card> myCards;

    public Person(String username, String email, String password_hash){
        this.id = genaretedLastId();
        this.username = username;
        this.email = email;
        this.password_hash = password_hash;
        this.stats = new Stats();
        this.myCards = new ArrayList<>();
    }



    public int genaretedLastId(){
        return lastId++;
    }

    public void setMyCards(List<Card> li){
        this.myCards = li;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password_hash;
    }

    public void setPassword(String password) {
        this.password_hash = password;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public String getAuthentication_token() {
        return authentication_token;
    }

    public void setAuthentication_token(String authentication_token) {
        this.authentication_token = authentication_token;
    }
}
