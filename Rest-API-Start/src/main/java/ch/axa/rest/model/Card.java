package ch.axa.rest.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Card {

    private static int lastid = 0;
    private int id;
    private String name;
    private double attribute1;
    private double attribute2;
    private double attribute3;
    private double attribute4;
    private double attribute5;
    private List<Double> attributes;
    private String imagePath;
    private List<String> names;



    public Card(String name, double attribute1, double attribute2, double attribute3, double attribute4,
                double attribute5, String imagePath, List<String> names){
        this.id = generatedLastId();
        this.name = name;
        this.attribute1 = attribute1;
        this.attribute2 = attribute2;
        this.attribute3 = attribute3;
        this.attribute4 = attribute4;
        this.attribute5 = attribute5;
        this.imagePath = imagePath;
        this.attributes = Arrays.asList(attribute1, attribute2, attribute3, attribute4, attribute5);
        this.names = names;
    }

    private int generatedLastId() {
        return lastid++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(double attribute1) {
        this.attribute1 = attribute1;
    }

    public double getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(double attribute2) {
        this.attribute2 = attribute2;
    }

    public double getAttribute3() {
        return attribute3;
    }

    public void setAttribute3(double attribute3) {
        this.attribute3 = attribute3;
    }

    public double getAttribute4() {
        return attribute4;
    }

    public void setAttribute4(double attribute4) {
        this.attribute4 = attribute4;
    }

    public double getAttribute5() {
        return attribute5;
    }

    public void setAttribute5(double attribute5) {
        this.attribute5 = attribute5;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<Double> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Double> attributes) {
        this.attributes = attributes;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }
}
