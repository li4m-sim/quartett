package ch.axa.rest.model;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import ch.axa.rest.AuthenticationData;
import ch.axa.rest.PasswordHashUtil;
import com.google.common.hash.Hashing;
import org.springframework.stereotype.Repository;

@Repository
public class PeopleRepositoryMock {
    private List<Person> people;
    private PasswordHashUtil phu;

    public PeopleRepositoryMock(){
        people = new ArrayList<>();

        people.add(new Person("liam", "liam@gmail.com", Hashing.sha256().hashString("1234", StandardCharsets.UTF_8).toString()));

        people.add(new Person("colin", "colin@gmail.com", Hashing.sha256().hashString("4544", StandardCharsets.UTF_8).toString()));

        people.add(new Person("alexander", "alex@gmail.com", Hashing.sha256().hashString("1313", StandardCharsets.UTF_8).toString()));
    }

    public Optional<String> authentication(AuthenticationData data) {

        for (Person p : people) {
            if (p.getUsername().equals(data.getUsername())) {
                if (p.getPassword().equals(Hashing.sha256().hashString(data.getPassword(), StandardCharsets.UTF_8).toString())) {
                    System.out.println("Richtiges Login");

                    p.setAuthentication_token(UUID.randomUUID().toString());

                    return Optional.of(p.getAuthentication_token());
                }
            }
        }
        return null;
    }


    public boolean isLogged(String token){
        for(Person p : people){
            if(p.getAuthentication_token() != null && p.getAuthentication_token().equals(token)){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    public boolean savePerson(Person person){
        if(checkIfPersonExists(person)){
            return true;
        }else {
            people.add(new Person(person.getUsername(), person.getEmail(), Hashing.sha256().hashString(person.getPassword(), StandardCharsets.UTF_8).toString()));
            return false;
        }
    }

    private boolean checkIfPersonExists(Person person) {
        Optional<Person> optionalPerson = people.stream()
                .filter(x -> x.getUsername() == person.getUsername() || x.getEmail() == person.getEmail())
                .findFirst();
        return optionalPerson.isPresent();
    }


}