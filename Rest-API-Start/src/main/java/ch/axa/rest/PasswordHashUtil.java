package ch.axa.rest;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class PasswordHashUtil {

    private String hash;

    public PasswordHashUtil(String pswrd){
        this.hash = Hashing.sha256()
                .hashString(pswrd, StandardCharsets.UTF_8)
                .toString();
    }

    public String getHashAsHexString(){
        return hash;
    }

    public boolean isPasswordMatching(String pswrd){
       return pswrd.equals(this.hash);
    }
}
