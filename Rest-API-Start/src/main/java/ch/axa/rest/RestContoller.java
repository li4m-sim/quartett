package ch.axa.rest;

import ch.axa.rest.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.DataLine;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class RestContoller {

    PeopleRepositoryMock peopleRepository;
    QuartettRepositoryMock quartettRepositoryMock;
    SoccerPlayerRepositoryMock soccerRepo;

    @Autowired
    public RestContoller(PeopleRepositoryMock peopleRepository, QuartettRepositoryMock quartettRepositoryMock) {
        this.peopleRepository = peopleRepository;
        this.quartettRepositoryMock = quartettRepositoryMock;
        this.soccerRepo = soccerRepo;
    }

    @GetMapping("/car-deck")
    public List<Card> getCars(){
        return quartettRepositoryMock.getCardsforUser("car");
    }

    @PostMapping("/card-deck")
    public List<Card> getCards(@RequestBody Type type){
        return quartettRepositoryMock.getCardsforUser(type.getType());
    }

    @GetMapping("/getPlayer")
    public List<Card> getPlayers(){
        return soccerRepo.getCardsCar();
    }

    @GetMapping("/car-deck/{id}")
    public Optional<Card> getCardById(@PathVariable int id){
        return quartettRepositoryMock.getCardById(id);
    }

    @PostMapping("/login")
    public ResponseEntity<Token> authenticateUser(@RequestBody AuthenticationData data) {
        Optional<String> optionalToken = peopleRepository.authentication(data);

        if (optionalToken != null) {
            System.out.println(optionalToken);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(new Token(optionalToken.get()));
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/newUser")
    public void newUser(@RequestBody Person person){
        peopleRepository.savePerson(person);
        System.out.println("Peson id" + person.getId() + " " + person.getUsername());

    }

    @PostMapping("/local-game")
    public List<List> playGame(@RequestBody Info info){
        System.out.println("action: " + info.getAction() + " attribute: " + info.getAttribute());
        return quartettRepositoryMock.play(info.getAction(), info.getAttribute());
    }

    @PostMapping("/pcPlays")
    public List<List> playPcClever(){
        return quartettRepositoryMock.playPcClever();
    }


}

class Token {
    private String token;

    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}